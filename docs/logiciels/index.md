# Python en ligne
<a href="https://serveur-pgdg.net/basthon-console/index.html" target="_blank">
<img src="basthon-console.svg" alt="Console Python" width="150px"/>
</a>&emsp;&emsp;&emsp;
<a href="https://serveur-pgdg.net/basthon-notebook/index.html" target="_blank">
<img src="basthon-notebook.svg" alt="NoteBook Python" width="150px"/>
</a>

# Geogebra
<a href="https://www.geogebra.org/calculator" target="_blank">
<img src="Geogebra-logo.svg" alt="Geogebra" width="100px"/>
</a>

# Xcas (calcul formel)
<a href="https://www-fourier.ujf-grenoble.fr/~parisse/xcasfr.html" target="_blank">
<img src="Xcaslogo.png" alt="Xcas" width="100px"/>
</a>

# Émulateur Numworks
<a href="https://www.numworks.com/fr/simulateur/" target="_blank">
<img src="numworks-ea17a340.svg" alt="Xcas" width="100px"/>
</a>

# Python à la maison sur ordinateur
<a href="https://thonny.org/" target="_blank">
<img src="thonny.png" alt="Thonny" width="100px"/>
</a>