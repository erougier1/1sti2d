# Programme de mathématiques en seconde générale

## Intentions majeures
La classe de seconde est conçue pour permettre aux élèves de consolider leur maîtrise du
    socle commun de connaissances, de compétences et de culture afin de réussir la transition
    du collège au lycée. Elle les prépare à déterminer leur choix d’un parcours au sein du cycle
    terminal jusqu’au baccalauréat général ou technologique dans l’objectif d’une poursuite
    d’études supérieures réussie et, au-delà, de leur insertion professionnelle.
    
L’enseignement des mathématiques de la classe de seconde est conçu à partir des
    intentions suivantes :

- permettre à chaque élève de consolider les acquis du collège et une culture
    mathématique de base, de développer son goût des mathématiques, d’en apprécier
    les démarches et les objets afin qu’il puisse faire l’expérience personnelle de
    l’efficacité des concepts mathématiques ainsi que de la simplification et de la
    généralisation que permet la maîtrise de l’abstraction ;
- préparer au choix de l’orientation : choix de la spécialité mathématiques dans la voie
    générale, choix de la série dans la voie technologique ;
- assurer les bases mathématiques nécessaires à toutes les poursuites d’études au
    lycée.

Le programme de mathématiques définit un ensemble de connaissances et de compétences
    qui s’appuie sur le programme de collège, en réactivant les notions déjà étudiées et en y
    ajoutant un nombre raisonnable de nouvelles notions, à étudier de manière suffisamment
    approfondie.

??? info "Compétences mathématiques"
    Dans le prolongement des cycles précédents, six grandes compétences sont travaillées :

    - **chercher**, expérimenter – en particulier à l’aide d’outils logiciels ;
    - **modéliser**, faire une simulation, valider ou invalider un modèle ;
    - **représenter**, choisir un cadre (numérique, algébrique, géométrique...), changer de
    registre ;
    - **raisonner**, démontrer, trouver des résultats partiels et les mettre en perspective ;
    calculer, appliquer des techniques et mettre en œuvre des algorithmes ;
    - **communiquer** un résultat par oral ou par écrit, expliquer une démarche.

    La résolution de problèmes est un cadre privilégié pour développer, mobiliser et combiner
    plusieurs de ces compétences. Cependant, pour prendre des initiatives, imaginer des pistes
    de solution et s’y engager sans s’égarer, l’élève doit disposer d’automatismes. Ceux-ci
    facilitent en effet le travail intellectuel en libérant l’esprit des soucis de mise en œuvre
    technique et élargissent le champ des démarches susceptibles d’être engagées.  
    L’acquisition de ces réflexes est favorisée par la mise en place d’activités rituelles,
    notamment de calcul (mental ou réfléchi, numérique ou littéral). Elle est menée
    conjointement avec la résolution de problèmes motivants et substantiels, afin de stabiliser
    connaissances, méthodes et stratégies.

??? info "Diversité de l’activité de l’élève"
    La mise en œuvre du programme doit permettre aux élèves d’acquérir des connaissances,
    des méthodes et des démarches spécifiques. 

    La diversité des activités concerne aussi bien les contextes (internes aux mathématiques ou
    liés à des situations issues de la vie quotidienne ou d’autres disciplines) que les types de
    tâches proposées : « questions flash » pour favoriser l’acquisition d’automatismes, exercices
    d’application et d’entraînement pour stabiliser et consolider les connaissances, exercices et
    problèmes favorisant les prises d’initiatives, mises au point collectives d’une solution,
    productions d’écrits individuels ou collectifs, etc.

    Il importe donc que cette diversité se retrouve dans les travaux proposés à la classe. Parmi
    ceux-ci, les travaux écrits faits hors du temps scolaire permettent, à travers l’autonomie
    laissée à chacun, le développement des qualités de prise d’initiative ou de communication
    ainsi que la stabilisation des connaissances et des méthodes étudiées. Ils doivent être
    conçus de façon à prendre en compte la diversité des élèves. Le calcul est un outil essentiel
    pour la résolution de problèmes. Il est important en classe de seconde de poursuivre
    l’acquisition d’automatismes initiée au collège. L’installation de ces automatismes est
    favorisée par la mise en place d’activités rituelles, notamment de calcul (mental ou réfléchi,
    numérique ou littéral). Elle est menée conjointement avec la résolution de problèmes
    motivants et substantiels, afin de stabiliser connaissances, méthodes et stratégies.

??? info "Utilisation de logiciels"
    L’utilisation de logiciels (calculatrice ou ordinateur), d’outils de visualisation et de
    représentation, de calcul (numérique ou formel), de simulation, de programmation développe
    la possibilité d’expérimenter, ouvre largement le dialogue entre l’observation et la
    démonstration et change profondément la nature de l’enseignement.

    L’utilisation régulière de ces outils peut intervenir selon trois modalités :
    
    - par le professeur, en classe, avec un dispositif de visualisation collective adapté ;
    - par les élèves, en classe, à l'occasion de la résolution d'exercices ou de problèmes ;
    - dans le cadre du travail personnel des élèves hors du temps de classe (par exemple
    au CDI ou à un autre point d’accès au réseau local).

??? info "Évaluation des élèves"
    Les élèves sont évalués en fonction des capacités attendues et selon des modalités variées :
    devoir surveillé avec ou sans calculatrice, devoir en temps libre, rédaction de travaux de
    recherche, individuels ou collectifs, compte rendu de travaux pratiques pouvant s’appuyer
    sur des logiciels, exposé oral d’une solution. L’évaluation doit permettre de repérer les
    acquis des élèves en lien avec les six compétences mathématiques : chercher, modéliser,
    représenter, raisonner, calculer, communiquer.

??? info "Place de l’oral"
    Les étapes de verbalisation et de reformulation jouent un rôle majeur dans l’appropriation
    des notions mathématiques et la résolution des problèmes. Comme toutes les disciplines, les
    mathématiques contribuent au développement des compétences orales, notamment à
    travers la pratique de l’argumentation. Celle-ci conduit à préciser sa pensée et à expliciter
    son raisonnement de manière à convaincre. Elle permet à chacun de faire évoluer sa
    pensée, jusqu’à la remettre en cause si nécessaire, pour accéder progressivement à la vérité
    par la preuve. Des situations variées se prêtent à la pratique de l’oral en mathématiques : la
    reformulation par l’élève d’un énoncé ou d’une démarche, les échanges interactifs lors de la
    construction du cours, les mises en commun après un temps de recherche, les corrections
    d’exercices, les travaux de groupe, les exposés individuels ou à plusieurs... L’oral
    mathématique mobilise à la fois le langage naturel et le langage symbolique dans ses
    différents registres (graphiques, formules, calcul).

??? info "Trace écrite"
    Disposer d’une trace de cours claire, explicite et structurée est une aide essentielle à
    l’apprentissage des mathématiques. Faisant suite aux étapes importantes de recherche,
    d’appropriation individuelle ou collective, la trace écrite récapitule de façon organisée les
    connaissances, les méthodes et les stratégies étudiées en classe. Explicitant les liens entre
    les différentes notions ainsi que leurs objectifs, éventuellement enrichie par des exemples ou
    des schémas, elle constitue pour l’élève une véritable référence vers laquelle il peut se
    tourner autant que de besoin. Sa consultation régulière (notamment au moment de la
    recherche d’exercices et de problèmes, sous la conduite du professeur ou en autonomie)
    favorise à la fois la mémorisation et le développement de compétences. Le professeur doit
    avoir le souci de la bonne qualité (mathématique et rédactionnelle) des traces écrites figurant
    au tableau et dans les cahiers d’élèves. En particulier, il est essentiel de bien distinguer le
    statut des énoncés (conjecture, définition, propriété - admise ou démontrée -, démonstration,
    théorème).

??? info "Travail personnel des élèves"
    Si la classe est le lieu privilégié pour la mise en activité mathématique des élèves, les
    travaux hors du temps scolaire sont indispensables pour consolider les apprentissages.
    Fréquents, de longueur raisonnable et de nature variée, ces travaux sont essentiels à la
    formation des élèves. Individuels ou en groupe, évalués à l’écrit ou à l’oral, ces travaux sont
    conçus de façon à prendre en compte la diversité des élèves et permettent le
    développement des qualités d’initiatives, tout en assurant la stabilisation des connaissances
    et des compétences.